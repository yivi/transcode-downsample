#! /usr/bin/env python
# -*- coding: utf-8 -* ...

import os
from os import path
import sys
import subprocess
import argparse
import gettext
import re
from shutil import copyfile

THREADS = 3

def downsample(dir, file, dir_output):
	output_file = path.join(dir_output, '%s.mp3' % file[:-4])
	return subprocess.Popen(['ffmpeg',
					'-i',
					path.join(dir, file),
					"-acodec", "libmp3lame", "-q:a", "6",
					output_file
					], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)


def onlyX(call, counter):
	if counter == THREADS:
		call.wait()
		return 0
	else:
		return counter + 1


def main():
	__ = gettext.gettext

	parser = argparse.ArgumentParser(description=__("Downsampling your files"), prog='Downsampler2k')

	parser.add_argument('--dir_source', '-d', help=__('Directory to scan') ,action='store', default=os.getcwd())
	parser.add_argument('--dir_output', '-o', help=__('Directory to write to'), action='store', default='./downsampled')

	parser.add_argument('--version, -v', action='version', version='%(prog)s 0.0001')

	args = parser.parse_args()


	counter = 0
	for root, dirs, files in os.walk(args.dir_source):

		thisDir = re.search(args.dir_source + "/?(.*)", root).group(1)
		targetDir = path.join(args.dir_output, thisDir)
		i = 0

		if not os.path.isdir(path.join(args.dir_output, thisDir)):
			os.makedirs(targetDir)

		for ifile in files:
			i = i +1
			if ifile.startswith('.'):
				continue

			elif path.isfile(path.join(targetDir, '%s.mp3' % ifile[:-4])):
				print "S: {}".format(ifile)

			elif ifile.lower().endswith('aac') or ifile.lower().endswith('m4a'):
				call = downsample(root, ifile, path.join(args.dir_output, thisDir))
				print "@: {}".format(ifile)
				if call: counter = onlyX(call, counter)

			elif ifile.lower().endswith('mp3'):
				call = subprocess.Popen(['ffmpeg', '-i', path.join(root, ifile)], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
				output = call.stdout.read()
				bitrate = re.search('Duration: \d\d:\d\d:\d\d\.\d\d, start: .*, bitrate: (\d{1,3}) kb/s', output).group(1)
				if int(bitrate) > 160:
					call = downsample(root, ifile, path.join(args.dir_output, thisDir))
					print "T: {}".format(ifile)
					if call:
						counter = onlyX(call, counter)
						# I do the thing
				else:
					## I copy the thing
					print "C: {}".format(ifile)
					copyfile(path.join(root, ifile), path.join(args.dir_output , thisDir , ifile))
			else:
				# log it somewhere
				print "#: WEIRD: {}".format(ifile)
				call = subprocess.Popen(['ffmpeg', '-i', path.join(root, ifile)], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

	return 0

if __name__ == '__main__':
	status = main()
	sys.exit(status)



